import styled from "styled-components";
import { Circle, Icon } from "./Option";
import checkIcon from "../img/checkIcon.png";
import cancelIcon from "../img/cancelIcon.png";
import resetIcon from "../img/redo.png";

const ContainerV1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top: 15px;
  padding-top: 25px;
  padding-bottom: 25px;
  border-top: 1px solid #ffffff;
`;

const ContainerV2 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

const Item = ({ v, onClick }) => {
  return (
    <Circle
      visible={true}
      style={{
        height: 60,
        width: 60,
        marginRight: 7,
        boxShadow: "0px 14px 4px -3px rgba(153,145,153,0.26)",
        WebkitBoxShadow: "0px 14px 4px -3px rgba(153,145,153,0.26)",
      }}
      onClick={onClick}
    >
      <Icon src={v} visible={true} style={{ height: 40, width: 40 }} />
    </Circle>
  );
};

const App = ({ version, onReset, onCancel, onCheck }) => {
  const handleCheck = () => {
    console.log("aceptado");
    if (typeof onCheck === "function") onCheck();
  };

  const handleReset = () => {
    console.log("reseteado");
    if (typeof onReset === "function") onReset();
  };

  const handleCancel = () => {
    console.log("cancelado");
    if (typeof onCancel === "function") onCancel();
  };

  if (version === 1) {
    /**
     * ! This is the orange card component
     */
    let data = [
      { icon: cancelIcon, onClick: handleCancel },
      { icon: resetIcon, onClick: handleReset },
      { icon: checkIcon, onClick: handleCheck },
    ];

    return (
      <ContainerV1>
        {data.map((v, i) => (
          <Item v={v.icon} onClick={v.onClick} />
        ))}
      </ContainerV1>
    );
  }

  if (version === 2) {
    /**
     * ! This is the green card component
     */
    let data = [
      { icon: cancelIcon, onClick: handleCancel },
      { icon: checkIcon, onClick: handleCheck },
    ];
    return (
      <ContainerV2>
        {data.map((v, i) => (
          <Item v={v.icon} onClick={v.onClick} />
        ))}
      </ContainerV2>
    );
  }

  return <></>;
};

export default App;
