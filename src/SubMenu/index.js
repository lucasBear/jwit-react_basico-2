import styled from "styled-components";
import Option from "./Option";
import Footer from "./Footer";

const Container = styled.div`
  background-color: ${(props) => props.color};
  border-radius: 25px;
  //height: 200px;
  width: 400px;
  margin-top: 10px;
  padding-top: 15px;
`;

const App = (params) => {
  /**
   * ? color: string;
   * ? options: string[];
   * ? footer: boolean;
   */

  const handleChange = (value) => {
    if (typeof params.onChange === "function")
      params.onChange(value, params.id);
  };

  const handleReset = () => {
    if (typeof params.onReset === "function") params.onReset(params.id);
  };

  return (
    <Container color={params.color}>
      {params.options.map((v, i) => (
        <Option key={i} {...v} onChange={handleChange} />
      ))}

      <Footer
        version={params.footer}
        onReset={handleReset} /*onCancel onCheck */
      />
    </Container>
  );
};

export default App;
