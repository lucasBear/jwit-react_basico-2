import React from "react";
import styled from "styled-components";
import checkIcon from "../img/checkIcon.png";

const Container = styled.div`
  margin-left: 25px;
  margin-top: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-right: 25px;
  align-items: center;
`;

const Title = styled.p`
  margin: 0px;
  color: white;
  font-weight: 600;
  text-transform: uppercase;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
    Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
`;

export const Circle = styled.div`
  border-radius: 50%;
  border: 1.8px solid white;
  height: 35px;
  width: 35px;
  justify-content: center;
  align-items: center;
  display: flex;
  cursor: pointer;
  background-color: ${(props) => (props.visible ? "white" : "transparent")};
`;

export const Icon = styled.img`
  height: 30px;
  width: 30px;
  object-fit: contain;
  border-radius: 50%;
  visibility: ${(props) => (props.visible ? "visible" : "hidden")};
`;

const Option = ({ title, state, onChange }) => {
  const handleChange = () => {
    if (typeof onChange === "function") onChange(title);
  };
  return (
    <Container>
      <Title>{title}</Title>
      <Circle visible={state} onClick={handleChange}>
        <Icon src={checkIcon} visible={state} />
      </Circle>
    </Container>
  );
};

export default Option;
