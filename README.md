# React Basico 2

This project was created following the course from Jwit platform.

[![basic-React2-Components-jwit.png](https://i.postimg.cc/Z52dFt80/basic-React2-Components-jwit.png)](https://postimg.cc/tsdJpcmH)

# React Basico 3

This project was created following the course from Jwit platform.

[![basic-React3-Components-jwit.png](https://i.postimg.cc/G2VVyjxp/basic-React3-Components-jwit.png)](https://postimg.cc/Q9gfL1PL)
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
